/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    // Set up audio functionality.
    audioDeviceManager.initialiseWithDefaultDevices (2, 2); // 2 inputs, 2 outputs.
    audioDeviceManager.addAudioCallback(this);
    
    // Set up MIDI functionality.
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback (String::empty, this);
    
    // Set up apmlitude slider.
    amplitudeSlider.setSliderStyle(juce::Slider::LinearVertical);
    amplitudeSlider.setRange(0.0, 1.0);
    amplitudeSlider.setValue(0.5);
    amplitudeSlider.addListener(this);
    addAndMakeVisible(&amplitudeSlider);

}

MainComponent::~MainComponent()
{
    // Stop listening to audio and midi callbacks.
    audioDeviceManager.removeAudioCallback(this);
    audioDeviceManager.removeMidiInputCallback (String::empty, this);
}

void MainComponent::resized()
{
    amplitudeSlider.setBounds(10, 10, 100, getHeight() - 20);
}

void MainComponent::audioDeviceIOCallback (const float** inputChannelData, int numInputChannels, float** outputChannelData, int numOutputChannels, int numSamples)
{
    DBG("'Audio Device IO Callback' called.\n");
    
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    // Process sample by sample.
    while (numSamples--)
    {
        *outL = *inL * amplitudeSlider.getValue();
        *outR = *inR * amplitudeSlider.getValue();
        
        inL++;
        inR++;
        outL++;
        outR++;
    }
}

void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("'Audio Device About To Start' called.\n");
}

void MainComponent::audioDeviceStopped()
{
    DBG("'Audio Device Stopped' called.\n");
}

void MainComponent::handleIncomingMidiMessage (MidiInput* source, const MidiMessage& message)
{
    DBG("MIDI message received.\n");
    
    // If message is from the modwheel...
    if (message.isController())
    {
        if (message.getControllerNumber() == 1)
            amplitudeSlider.getValueObject().setValue (message.getControllerValue() / 127.0);   // Scale value and update the slider.
    }
}

void MainComponent::sliderValueChanged (Slider* slider)
{
    if (&amplitudeSlider == slider)
        DBG("Slider value changed.\n");
}